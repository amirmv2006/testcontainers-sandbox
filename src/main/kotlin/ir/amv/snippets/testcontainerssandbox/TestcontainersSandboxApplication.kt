package ir.amv.snippets.testcontainerssandbox

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestcontainersSandboxApplication

fun main(args: Array<String>) {
    runApplication<TestcontainersSandboxApplication>(*args)
}
